#!/bin/bash
plugin=$1
for a in `seq 1 1`;
do
    echo $a
    manage.py synopsis_plugin_load -p custom_conf/$1/metadata > stdout
    if [[ $? == 1 ]]; then
        file=$(cat stdout | tail -fn1 | cut -d ' ' -f 4)
        file=$(basename file) 
        echo ${file} >> files_list_error_$1
        find custom_conf/ -type f -name "${file}" -exec rm {} \; 
    else
        exit 0
    fi
done

