
option=$1
input_file=$2
output_file=$3


if [[ $@ -neq 3 ]]
then
	echo 'pdf compressor need 3 parameters in input cmd'
	echo '[OPTION|INPUTFILE|OUTPUTFILE]'
	echo 'options availables prepress ebook and screen'
fi


gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/$option -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$output_file $input_file

