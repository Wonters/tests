from flask import Flask
from flask import render_template, request, session, url_for, jsonify
from mlg import httpcli
import os
import re


app = Flask(__name__)


@app.route('/')
def ui():
    return render_template('ui.html', results={})


@app.route('/urls')
def test_urls():
    with open('../endpoints_files/endpoints', 'r') as f:
        urls = f.read().split("\n")

    creds = ["hoberlin", "hoberlin"]
    hosts = {"chips": "10.0.8.221:8080", "omia": "10.0.11.251:8080", "socle": "127.0.0.1:8080"}

    client = httpcli.OmiaHttpClient(creds=creds, host=hosts.get("socle"))
    pattern_url = re.compile("^.*\(.*\).*$")
    params = {}
    results = {}
    for url in urls:
        if re.match(pattern_url, url) == None:
            response = client.requestor.get('http://' + client.host + '/' + url,
                                            # headers=headers,
                                            params={},
                                            timeout=20)
            if response.status_code != 200:
                results[url] = response.status_code
                # logger.warning("{0}".format(response.headers))

    return render_template('ui.html', results=results)