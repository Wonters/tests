import functools
import logging
import sqlite3
from flask import (
    Blueprint, flash, g, redirect, render_template,
    request, session, url_for, jsonify
)

logger = logging.getLogger(__name__)
logger.setLevel(level=logging.INFO)
from .db import get_db

bp = Blueprint('atoms', __name__, url_prefix='/atoms')


def get_element_table():
    db = get_db()
    data = db.execute(
        'SELECT * FROM atome'
    ).fetchall()
    return data


@bp.route('/mendeleiv')
def mendeleiv():
    data = get_element_table()
    return render_template('mendeleiv.html', data=data)

@bp.route('/serialize')
def serialize():
    data = get_element_table()
    atoms = list()
    for el in data:
        atom = {}
        for key in el.keys():
            atom[key] = el[key]
        atoms.append(atom)
    return jsonify(atoms)

@bp.route('/tableau')
def tableau():
    rows = 7
    cols = 18
    return render_template('periodic_table.html',rows=rows, cols=cols)
