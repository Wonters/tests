class Base:
    def jetest(self, *args, **kwargs):
        print("init Base")


class Test(Base):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def jetest(self, *args, **kwargs):
        print("debut de get capabilities")
        print("suite")


class Mixin:

    def jetest(self, *args, **kwargs):
        print("mixins prend le dessus")
        super().jetest(*args, **kwargs)


class WCSTest(Mixin, Test):

    def __init__(self, *args, **kwargs):
        print("init de WCSTest")
        super().__init__(self)

    def on_start(self):
        self.jetest()


if __name__ == "__main__":
    wcs = WCSTest()
