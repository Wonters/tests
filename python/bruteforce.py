import numpy as np
import itertools
import random
import re
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
# create a random password(size)
def create_password(size)
    password = str()
    for element in range(size):
        password.join(chr(random.randrange(64,122)))
    return password

# read the usedful password(file, password)
def find_from_file(file, pwd ):
    response = None
    with open(file, "r") as f:
        while f.readline():
            pwd_test = re.findall("^.*[^\n]$",f.readline())
            if len(pwd_test) != len(pwd):
                logger.warning(f"The len of password in the file is different from th pwd len in input")
        if pwd_test == pwd or pwd_test == lower(pwd) :
            logger.info(f"password is found: {pwd_test}")
            response = pwd_test
    return response

# find the password (password, size)
def brute_force(password):
    """
    TODO Make an algorithme to find password
    :param password:
    :return:
    """
    file_pwd = "simple_pwd.txt"
    simple_pwd = find_from_file(file_pwd, password)
    if simple_pwd != None:
        logger.info(f"PASSWORD IS {simple_pwd}")
    else:
        for element in password:
            re.match("",element)
