import threading
from python.common import multiprocessing
import os
import time
import threading


def process_(l):
    s = 0
    for i in range(4):
        s += i
    print("process:{0} thread:{1}".format(os.getpid(), threading.get_ident()))


def thread_(param_tuple):
    s = 0
    for i in range(4):
        s += i
    print("process:{0} thread:{1}".format(os.getpid(), threading.get_ident()))


l = [1, 2, 3, 4, 54, 6]

start = time.time()
for a in range(3):
    p = multiprocessing.Process(target=process_, args=(a,))
    p.start()
    # p.join()  // à quoi sert ce putain de join
stop = time.time()
print("Process teminé en {0}".format(stop - start))

start = time.time()
for a in range(3):
    t = threading.Thread(target=thread_, args=(a,))
    t.start()
stop = time.time()
print("Thread terminé en {0}".format(stop - start))

start = time.time()
for a in range(3):
    s = 0
    for i in l:
        s += i
    # print('classic do : {0}'.format(i))
stop = time.time()
print('classic terminé en {0}'.format(stop - start))
