from locust import TaskSet, task, between, HttpLocust
from multiprocessing import Process

class BrowseDocumentation(TaskSet):
    def on_start(self):
        self.index_page()
        self.urls_on_current_page = self.toc_urls


    @task(10)
    def index_page(self):
        r = self.client.get("/")

import django
