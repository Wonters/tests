import multiprocessing
import time
import os
import requests

class WebsiteError():
    pass

def ping_website(address):
    q=requests.head(address)
    if q.status_code >= 402:
        print("WebsiteError")
        ping_website()
        raise WebsiteError
    else:
        print("Working ...")

def check_website(address):
    print('check web site with :{0}'.format(address))
    try:
        ping_website(address)
        print("ping websites")
    except requests.exceptions.ChunkedEncodingError or requests.exceptions.RequestException as err:
        print("error from ping website: {0}".format(err))
WEBSITES=['google.com','celery.com','youtube.com']


NUM_WORKER=4
start = time.time()
with multiprocessing.Pool(processes=NUM_WORKER) as p:
    p.map(check_website, WEBSITES)
stop = time.time()

print("Process finish with time {0}".format(stop - start))
