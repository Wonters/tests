import requests
import bs4

if __name__ == '__main__':
    url = 'http://google.com'
    response = requests.get(url)
    soup = bs4.BeautifulSoup(markup=response.content, features="lxml")
    print(soup.a["href"])
    print(soup.a["class"])
    print(soup.p.a)
    print("{0}".format([parent.name for parent in [el.parents for el in soup.find_all("p")][0]]))

class ParsingHtml:

    def __init__(self, url):
        response = requests.get(url)
        soup = bs4.BeautifulSoup(markup=response.content, features="lxml")
        self.headers = response.headers
        self.cookies = response.cookies

    def find_a_links(self):
        links = list()
        for a in soup.find_all('a'):
            links.append(a['href'])
        return links

    def find_js_tools(self):
        """
        :arg
        Return js tools used in the web site
        """
