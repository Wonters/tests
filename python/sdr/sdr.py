from matplotlib import pyplot
import numpy as np
from pydub import AudioSegment
from pydub.utils import get_array_type
import scipy.io.wavfile as wav
from scipy.fft import fft


class GenerateRadioSignal:
    """ 
    Object handle to modulate an audio signal to send it on carrier with a frequency set
    """

    def __init__(self, file):
        self.rate = 1  # sampling acquisition
        self.mono = False  # if mono or stereo 
        self.audio_signal = np.array([])  # audio signal
        self.times = np.linspace(0, 20, 256)
        self.set_signal_from_audio(audio_file=file, channel='left')

    def set_signal_from_audio(self, audio_file, channel):
        """ Open a wav file and return durarition, mono piste, rate"""
        audio = wav.read(audio_file)
        self.rate = audio[0]
        if channel == 'left':
            data = np.array(audio[1][:, 0])
        else:
            data = np.array(audio[1][:, 1])
        self.audio_signal = data / data.max()
        self.times = np.linspace(0., data.shape[0] / self.rate, data.shape[0])

    def get_am_modulation_signal(self, carrier_freq):
        """ Return a modulation of the signal on a frequency set, with noise"""
        gain = 1
        Ap = 20
        signal_test = 0.5*np.sin(2 * np.pi * 1000 * self.times)
        carrier = Ap * np.sin(2 * np.pi * carrier_freq * self.times)
        mod_signal = carrier + gain * carrier * self.audio_signal
        return mod_signal

    def get_spectrum(self, signal):
        sprectrum = fft(signal)
        return sprectrum.real

    def show_modulation(self):
        F = 10000
        N = len(self.times)
        duration = self.times.max() - self.times.min()
        T = duration / N
        signal = self.get_am_modulation_signal(carrier_freq= F)
        spectrum = self.get_spectrum(signal)
        fig, (ax1, ax2) = pyplot.subplots(1, 2)
        ax1.plot(self.times, signal)
        ax1.set(xlabel='time (s)', ylabel='voltage (mV)',
                title='Modulated signal')
        ax1.grid()
        axe_f = np.linspace(0.0, 1 / T, N)
        ax2.plot(axe_f, 2 / N * np.abs(spectrum))
        ax2.set(xlabel="frequency (Hz)", ylabel='FFT', title="Audio spectrum")
        ax2.grid()
        ax2.set_xlim(F - F/1, F + F/1 )
        pyplot.show()


generator = GenerateRadioSignal('../../output.wav')
generator.show_modulation()
