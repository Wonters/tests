from matplotlib import pyplot
import numpy as np
from scipy.fft import fft, fftfreq

# FREQ = 100
# SIZE = 2560
# SAMPLE_SPACING = 0.1
# times = np.linspace(0, SIZE*SAMPLE_SPACING, SIZE)
# signal = np.sin(2 * np.pi * FREQ * times)  # todo: modulation am
# spectrum = np.abs(fft.fft(np.sin(times)))
# frequency = fft.fftfreq(SIZE)
#
# fig, (ax1, ax2) = pyplot.subplots(1, 2)
# ax1.plot(times, signal)
# ax1.set(xlabel='time (s)', ylabel='voltage (mV)',
#         title='Audio signal')
# ax1.grid()
# ax2.plot(frequency, spectrum)
# ax2.set(xlabel="frequency (Hz)", ylabel='FFT', title="Audio spectrum")
# ax2.grid()
# pyplot.show()

# Number of sample points
N = 3000
# sample spacing
T = 1.0 / N
x = np.linspace(0.0, N*T, N)
y2 = 10*np.sin(2*np.pi*100*x)
y1 = 5*np.sin(50 * 2.0*np.pi*x)
y = y1 + y2
yf = fft(y)
xf = np.linspace(0.0, 1.0/(T), N)
pyplot.plot(xf, 2/N * np.abs(yf[0:N]), x, y)
pyplot.grid()
pyplot.show()