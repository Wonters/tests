import pytest
import numpy as np
from .sdr import *
import scipy.integrate as intg
LENGTH = 2  # second
SAMPLING_FREQUENCY = 44600
FREQUENCY = 0.5


@pytest.fixture(scope='function')
def signal():
    """ Return a sin signal """
    times = np.arange(0., LENGTH, 1 / SAMPLING_FREQUENCY)
    signal = np.sin(2 * np.pi * FREQUENCY * times)
    return signal


@pytest.mark.parametrize("freq_carrier", [50, 100, 500, 1000])
def test_modulation(signal, freq_carrier):
    """ Test the modulation step to test the amplitude"""
    mod_signal = module_signal(signal, freq_carrier)
    intg.trapz(mod_signal)
    assert 0


def test_plot_spectre(signal, data):
    """ Test plot show for a data """

    assert 0


def test_fft(frequencies, signal):
    # test fft return frequencies in the signal
    assert 0


def test_filter(signal):
    assert 0


def bode_(signal):
    # test function bode return right gain, phase, cuting frequency, order, bandwidth 
    assert 0
