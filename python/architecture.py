class A:
    toto = "tata"

    def __init__(self):
        print("initiatilisation")


    def a(self):
        print('function a')

    @classmethod
    def _class_function(cls):
        print(cls.toto)

class B(A):
    def __init__(self):
        super(B,self).__init__()
        print("class B")

class MyDict(dict):

    def __init__(self):
        pass



example = A()
example._class_function()


A._class_function()

b = B()
b.a()

print(isinstance("toto", basestring))

