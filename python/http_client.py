import requests


class Client:
    schema_http = "http://"

    def __init__(self, host, port, user, password):
        self.host = host
        self.port = port
        self.password = password
        self.user = user
        self.full_schema = f"{self.schema_http}{self.host}:{self.port}" if port is not "" else f"{self.schema_http}{self.host}"

    def auth(self):
        """ Authenticate on the api"""
        response = requests.get(f"{self.full_schema}")
        token = response.cookies['csrftoken']
        return requests.post(f"{self.full_schema}/connexion",
                             params={"username": self.user, "password": self.password,
                                     "middlewaretoken": token})

    def get(self, upath, **kwargs):
        """ Get an upath on the site"""
        return requests.get(f"{self.full_schema}/{upath}", params=kwargs.get("params"))


class PluginMixin:

    def pluginUn(self):
        class PluginUn:
            def url1(self):
                return self.get("url1")
                pass

            def url2(self):
                return self.get("url2")
                pass

        return PluginUn

    class PuginDeux:
        def url3(self):
            self.get("url3")
            pass


class PluginMixin2:
    def url1(self):
        return self.get("url1")

    def url2(self):
        return self.get("url2")


class AineaClient(PluginMixin, Client):
    """ Client Ainea """
    pass


client = AineaClient(host="questionnaire.ainea.fr/ainea/", port="", password="Tri12098",
                     user="tristan")


