class A:

    def __init__(self):
        self.variable = 1
        print("initialization")

    @property
    def context_manager(self):
        class Manager(A):
            def __enter__(self):
                print("enter in context")
                print(self.variable)

            def __exit__(self, exc_type, exc_val, exc_tb):
                print("quit the context")

        return Manager()

    def need_context(self):
        with self.context_manager:
            print("Do someting in the context manager")


e = A()
e.need_context()
