import subprocess as sp
from datetime import datetime as date 

tmp_path = "/tmp/test_process.log"

with open(tmp_path, "a+") as tmp_output:
    tmp_output.write('{date.now().strftime("%H:%M:%S")}\n')
    process = sp.Popen('sleep 4', stdout=tmp_output, stderr=tmp_output, shell=True)
    process.wait()
    tmp_output.write(date.now().strftime("%H:%M:%S"))

print('process started')

