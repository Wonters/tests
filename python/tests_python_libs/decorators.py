class Decorator:

    def __init__(self, method):
        self.method = method

    def __call__(self, func):
        def wrapper(*args, **kwargs):
            print("Do something before the fonction")
            print(self.method)
            print(args, kwargs)
            result = func(*args, **kwargs)
            print("Do something after the function")
            return result

        return wrapper


@Decorator(method="POST")
def do_something(message="bonjour les pixels"):
    print(message)
    pass


def de0(func):
    def wrapper0(*args):
        print(f"before {func.__name__}")
        func(*args)
        print(f"after {func.__name__}")
        return 0

    return wrapper0


def de1(func):
    @de0
    def wrapper1(*args):
        print(f"before {func.__name__}")
        func(*args)
        print(f"after {func.__name__}")
        return 0

    return wrapper1


@de1
def do_an_other_thing():
    print("do an other thing")


do_something(message="il faut travailler")
print("\n")
do_an_other_thing()
