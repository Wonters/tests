import svgwrite as sw

dwg = sw.Drawing('test.svg', profile='tiny')
dwg.add(dwg.line((0, 0), (10, 5), stroke=sw.rgb(10, 10, 16, '%')))
dwg.add(dwg.text('Th', insert=(0, 10), fill='red'))
dwg.add(dwg.line((0, 0), (10, 5), stroke=sw.rgb(10, 10, 16, '%')))
dwg.save()