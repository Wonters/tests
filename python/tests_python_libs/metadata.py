class M(type):
    """
    type can give type of an object or create a new object as type('name', (), {})
    equivalent to
    class name:
        pass
    """

    def __new__(cls, name, bases, dict):
        return super().__new__(cls, name, bases, dict)

    @classmethod
    def __prepare__(metacls, name, bases):
        return {'test': lambda self: print(f"instance {self}", f"object {name}", metacls)}


class A(metaclass=M):
    pass


print("object create and not instantiate", A)
print("type of object A", type(A))
A().test()

"""
Understand the Enum metaclass
"""


class EnumMeta(type):
    def __new__(cls, name, bases, dict):
        print("New metaclass")
        # Cache dans lequel les instances seront stockeées
        dict['__mapping__'] = {}
        members = {k: v for (k, v) in dict.items() if not (k.startswith('__') and k.endswith('__'))}
        enum = super().__new__(cls, name, bases, dict)
        for key, value in members.items():
            value = enum(value)
            value.name = key
            setattr(enum, key, value)
        return enum


class Enum(metaclass=EnumMeta):
    def __new__(cls, value):
        if value in cls.__mapping__:
            return cls.__mapping__(value)
        v = super().__new__(cls)
        v.value = value
        v.name = ''
        # On l'ajoute au cache
        cls.__mapping__[value] = v
        return v

    def __repr__(self):
        return '<{}.{}: {}>'.format(type(self).__name__, self.name, self.value)


class B(Enum):
    e = 'einstein'
    c = "charlie parker"
    n = 2


print(B.e, B.n)


# enum = B(value=4)
# print(enum.e, enum.n, enum.value, enum.name)
#

class R:
    def __new__(cls, *args, **kwargs):
        """New must return an object"""
        print("new")
        return super().__new__(cls, *args, **kwargs)

    def __init__(self):
        print("init")


print(R)
R()
