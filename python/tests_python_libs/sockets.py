import socket
import os
from multiprocessing import Process

# create an INET, STREAMing socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# now connect to the web server on port 80 - the normal http port
s.connect(("www.python.org", 80))

pid = os.fork()

if pid > 0:
    # create an INET, STREAMing socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as serversocket:
        # bind the socket to a public host, and a well-known port
        serversocket.bind(("127.0.0.1", 9080))
        # become a server socket
        serversocket.listen()
        clientsocket, address = serversocket.accept()
        print("server is connected ")
        with clientsocket:
            while True:
                data = clientsocket.recv(1024)
                print(data)
                if not data:
                    break
                # accept connections from outside
                # now do something with the clientsocket
                # in this case, we'll pretend this is a threaded server
                clientsocket.sendall(b"Welcom to the server")
else:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(("127.0.0.1", 9080))
        s.sendall(b"Hello, world")
        data = s.recv(1024)

    print(f"Received {data!r}")

