import socket
import os
import time
from functools import partial
import multiprocessing as mp

from multiprocessing import Process, Lock


def f(i):
    # l.acquire()
    #  try:
    print('hello world', i)


#  finally:
#      l.release()


m = mp.Manager()
queue = m.Queue(maxsize=100)


def add(q, a, b):
    print("addition of numbers")
    time.sleep(1)
    queue.put(f"a message for you: {a + b}")
    return a + b


if __name__ == '__main__':
    # lock = Lock()

    for num in range(10):
        Process(target=f, args=(num,)).start()

# Thread and process
# One process contains multiples threads
# CPU physic * sockets * num_thread === CPU virtuals

# start = time.time()
# with mp.Pool(8) as pool:
#     pool.starmap(partial(add, queue), [(a, a) for a in range(5)])
#
# stop = time.time()
#
# print(f"Done in {stop - start}s")
# while not queue.empty():
#     print(queue.get())
