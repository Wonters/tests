import numpy as np


def test_a():
    x = np.random.randint(1, 3, size=(2, 3))
    y = np.random.randint(1, 3, size=(2, 3))
    m = np.sqrt(x ** 2 + y ** 2)
    min_y_index = np.where(y == y.min())
    min_x_index = np.where(x == x.min())
    min_m_index = np.where(m == m.min())
    mat_min_y_index = np.array(list(zip(min_y_index[0], min_y_index[1])))
    mat_min_x_index = np.array(list(zip(min_x_index[0], min_x_index[1])))
    print(mat_min_y_index, mat_min_x_index)
    print(mat_min_y_index & mat_min_x_index)
    print("y", y)
    print("x", x)
    print("m", m)
    print(f"min y: {y.min()} at {min_y_index}")
    print(f"min x: {x.min()} at {min_x_index}")
    print(f"min m: {m.min()} at {min_m_index}")


def test_b():
    x = np.array([[0, 1], [1, 2]])
    y = np.array([[1, 1], [1, -1]])
    print(x)
    print(y)
    print(x @ y)


# test_b()
test_a()
