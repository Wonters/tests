from django.db import models

# Create your models here.

class cleam(models.Model):
    temperature = models.IntegerField(default=0)
    activate = models.BooleanField(default=0)


class car(models.Model):
    color = models.CharField(default='blue', max_length=30)
    brand = models.CharField(default='peugeot', max_length=30)
    option_cleam = models.ForeignKey(cleam, on_delete=models.CASCADE)
