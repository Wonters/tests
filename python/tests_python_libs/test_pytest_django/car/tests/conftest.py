import sqlite3
import pytest
from django.conf import settings

from options.models import car, cleam

def run_sql(sql):
    conn = sqlite3.connect(database=str(settings.DATABASES['default']['NAME']))
    cur = conn.cursor()
    query = cur.execute(sql)
    return query


@pytest.fixture
def db_all():
    res = run_sql('select * from options_cleam;')
    return res.fetchall()


# @pytest.fixture(scope='session')
# def django_db_setup():
#     print('django setup database')
#     pass


@pytest.fixture(scope='session')
def django_db_keepdb():
    pass


@pytest.fixture(scope='session')
def django_db_modify_db_settings():
    pass

@pytest.fixture
def get_car_from_db():
    cars = car.objects.all()
    print(cars)
