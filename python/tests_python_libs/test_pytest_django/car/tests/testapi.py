import pytest
from faker import Faker

fake = Faker()

from options.models import car, cleam
from django.contrib.auth.models import User


def test_db(django_user_model):
    user = django_user_model.objects.create(username='tristan')
    user.save()
    print(django_user_model.objects.all())


@pytest.mark.django_db
def test_create_car():
    for _ in range(10):
        new_option_cleam = cleam(temperature=fake.random_int(max=20), activate=fake.boolean())
        new_option_cleam.save()

        new_car = car(brand=fake.first_name_male(), color=fake.color(),
                      option_cleam=new_option_cleam)
        new_car.save()
    print(car.objects.all())


def test_create_users(db):
    user = User.objects.create_user(username=fake.first_name_male())
    print(user)


@pytest.mark.django_db()
def test_get_all_users():
    print(User.objects.all())


@pytest.mark.django_db
def test_get_all_cleam():
    cleams = cleam.objects.all()
    print(cleams)


@pytest.mark.django_db()
def test_get_all_cars(get_car_from_db):
    print(get_car_from_db)


def test_request_db(db_all):
    a = db_all
    print('database result', a)
