import bs4
import glob
import click
import numpy as np
import asyncio
import time
from aiofile import AIOFile


async def get_text_file_async(file: str):
    """ test asynchrone """

    print(f"file {file} read")
    async with AIOFile(file, "r") as f:
        html = await f.read()
        ts = time.time()
        soup = bs4.BeautifulSoup(markup=html, features="lxml")
        te = time.time()
        print(f"parsing html in {te - ts} s", )
        test_passed = soup.find(class_="passed").text
        test_failed = soup.find(class_="failed").text
    return np.array([int(test_passed.split(' ')[0]), int(test_failed.split(' ')[0])])


@click.command()
@click.option("--path", required=True, type=click.Path(exists=True, dir_okay=True))
def async_test(path):
    files = glob.glob(path + "/*.html")
    loop = asyncio.get_event_loop()
    ts = time.time()
    tasks = loop.run_until_complete(asyncio.wait([get_text_file_async(file) for file in files]))
    te = time.time()
    print(f"function async execution time : {te - ts}")


# Exemple from zeste de savoir website
async def perf_test(nb_requests, period, timeout):
    tasks = []
    # On lance 'nb_requests' commandes à 'period' secondes d'intervalle
    for idx in range(1, nb_requests + 1):
        client_name = "client_{}".format(idx)
        tsk = asyncio.ensure_future(get_text_file_async(client_name))
        tasks.append(tsk)
        await asyncio.sleep(period)

    finished, _ = await asyncio.wait(tasks)
    success = set()
    for tsk in finished:
        if tsk.result().seconds < timeout:
            success.add(tsk)

    print("{}/{} clients satisfaits".format(len(success), len(finished)))


if __name__ == "__main__":
    async_test()
