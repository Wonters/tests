from fastapi import FastAPI, Request, Response
from fastapi.templating import Jinja2Templates
import folium as f
from matplotlib import cm
from matplotlib import pyplot as plt
from mpl_toolkits.basemap import Basemap
import xarray as xr
import cartopy.crs as ccrs
import matplotlib.animation as animation
import ipyvolume as ipv
import requests
import numpy as np
import pandas as pd

app = FastAPI()
templates = Jinja2Templates('templates')


@app.get('/map/')
def map(request: Request):
    """ Retrieve a html map from folium and leaflet """

    map = f.Map()
    return Response(map._repr_html_())


@app.get('/earth')
def earth(request: Request):
    data = pd.read_csv("https://data.giss.nasa.gov/gistemp/tabledata_v4/GLB.Ts+dSST.csv")
    map = Basemap()
    # df = xr.open_dataset('https://data.giss.nasa.gov/pub/gistemp/landmask.2degx2deg.txt')
    lon = np.arange(-180, 180, 0.02)  # df.lon.values
    lat = np.arange(-90, 90, 0.01)  # df.lat.values
    z = np.arange(0, 180, 1)  # df.z.values

    VALUES = np.zeros(lat.shape, dtype=np.float64)
    znorm = VALUES - VALUES.min()
    znorm /= znorm.ptp()
    znorm.min(), znorm.max()
    colormap = cm.jet
    color = colormap(znorm)
    ipv.figure()
    x, y, z, r, theta, phi = xyz()
    print([a.shape for a in (x, y, z, r, phi, theta)])
    mesh = ipv.plot_surface(x, y, z, color='red')
    # countries = ipv.pylab.plot(xs, ys, zs, color='black')
    ipv.pylab.style.box_off()
    ipv.pylab.style.axes_off()
    ipv.pylab.save("templates/ipyvolume_sphere3D.html")
    return templates.TemplateResponse("ipyvolume_sphere3D.html", {'request': request})


def xyz(shape=128, limits=[-3, 3], spherical=True, sparse=False, centers=True):
    dim = 3
    try:
        shape[0]
    except:
        shape = [shape] * dim
    try:
        limits[0][0]  # pylint: disable=unsubscriptable-object
    except:
        limits = [limits] * dim
    if centers:
        v = [
            slice(vmin + (vmax - vmin) / float(N) / 2, vmax - (vmax - vmin) / float(N) / 4,
                  (vmax - vmin) / float(N))
            for (vmin, vmax), N in zip(limits, shape)
        ]
    else:
        v = [
            slice(vmin, vmax + (vmax - vmin) / float(N) / 2, (vmax - vmin) / float(N - 1))
            for (vmin, vmax), N in zip(limits, shape)
        ]
    if sparse:
        x, y, z = np.ogrid.__getitem__(v)
    else:
        x, y, z = np.mgrid.__getitem__(v)
    if spherical:
        r = np.linalg.norm([x, y, z])
        theta = np.arctan2(y, x)
        phi = np.arccos(z / r)
        return x, y, z, r, theta, phi
    else:
        return x, y, z
