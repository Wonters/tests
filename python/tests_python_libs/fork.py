import os


pid = os.fork()
if pid > 0:
    print("parent process")
    print(pid)
    print(os.getpid())
else:
    print(os.getpid())
    print("children process")