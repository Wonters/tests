class A:
    fafa = "fafa attribute"
    fifi = "fifi attribute"

    def __init__(self, **kwargs):
        self.tata = "tata"
        for k, v in kwargs.items():
            setattr(self, k, v)
        print('initialisation')

    @classmethod
    def att(cls, **kwargs):
        def view(*args):
            self = cls()
            self.args = args
            return self.first_method(*args)
        print(dir(view))
        view.cls = cls
        print(dir(view))

        return view

    def first_method(self, *args):
        print("first method")
        print(self.fafa)
        print(self.fifi)
        print(*args)
        return 1

    @classmethod
    def second_method(cls, *args):
        print("second method launch with cls attribute")


func = A.att()
# cls is a mutable object
# don't need to redefine it
transition_cls = func.cls
transition_cls.fafa = "change fafa eee"
transition_cls.fifi = "change fifi"
print(func())
transition_cls.second_method()

