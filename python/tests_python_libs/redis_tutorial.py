"""
This file follow the realpython tutorial found in this url : https://realpython.com/python-redis/
"""

import redis
import random
import logging

logging.basicConfig()

r = redis.Redis(db=1)


class OutOfStockError(Exception):
    """Raised when PyHats.com is all out of today's hottest hat"""


def buyitem(r: redis.Redis, itemid: str):
    with r.pipeline() as pipe:
        error_count = 0
        while True:
            try:
                # Get available inventory, watching for changes
                # related to this itemid before the transaction
                pipe.watch(itemid)
                nleft: bytes = r.hget(itemid, "quantity")
                if nleft > b'0':
                    pipe.multi()
                    pipe.hincrby(itemid, "quantity", -1)
                    pipe.hincrby(itemid, "npurchased", 1)
                    pipe.execute()
                    break
                else:
                    # Stop watching the itemid and raise to break out
                    pipe.unwatch()
                    raise OutOfStockError(
                        f"Sorry, {itemid} is out of stock!"
                    )
            except redis.WatchError:
                # Log total num. of errors by this user to buy this item,
                # them try the same process again of WATCH/HGET/MULTI/EXEC
                error_count += 1
                logging.warning("WatchError #%d : %s; retrying", error_count, itemid)

    return None


def create_database():
    random.seed(444)
    hats = {f'hat:{random.getrandbits(32)}': i for i in (
        {
            "color": "black",
            "price": "49.99",
            "style": "fitted",
            "quantity": 1000,
            "npurchased": 0,
        },
        {
            "color": "black",
            "price": "59.99",
            "style": "fitted",
            "quantity": 500,
            "npurchased": 0,
        },
        {
            "color": "black",
            "price": "99.99",
            "style": "fitted",
            "quantity": 200,
            "npurchased": 0,
        }
    )}

    with r.pipeline() as pipe:
        for h_id, hat in hats.items():
            pipe.hmset(h_id.encode(), hat)
        pipe.execute()

    r.bgsave()


id = "hat:56854717"
for _ in range(196):
    buyitem(r, id)

print(r.hmget(id, "quantity", "npurchased"))
