import asyncio
import datetime
import time

FRITES = 0


async def soda():
    print("soda is process")
    async with asyncio.Lock():
        await asyncio.sleep(1)
        print("soda in ready")


async def hamburger():
    print("hamburger is starting")
    async with asyncio.Semaphore(3):
        await asyncio.sleep(2)
        print("hamburger is ready")


async def frites():
    global FRITES
    print("frites starting")
    async with asyncio.Lock():
        if FRITES == 0:
            FRITES = 5
            await asyncio.sleep(5)
            print("frites ready")
        FRITES -= 1


async def serve(client: str):
    print(f"client {client} is strating ")
    ts = time.time()
    await asyncio.wait(
        [soda(),
         hamburger(),
         frites(),
         ]
    )
    te = time.time()
    print(f'client {client} serve in {te - ts}')


loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.wait([serve(c) for c in "abcde"]))
