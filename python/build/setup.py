import setuptools


setuptools.setup(
        name="test_package",
        version="1.0.0",
        author="wonters",
        description="a fake package to test pypiserver",
        packages=setuptools.find_packages(),
        classifiers=[
            "Programming Language :: Python :: 3",
            "Licence :: OSI Approved :: MIT Licence",
            "Operatiog System :: OS Independent",
            ],
        python_requires='>=3.6'
        )
