import re
import sys
import os
import typing
from mitmproxy.tools.main import assert_utf8_env, run, cmdline
from mitmproxy.tools import console
from mitmproxy.tools import dump


def mitmproxy(args=None) -> typing.Optional[int]:  # pragma: no cover
    assert_utf8_env()
    run(console.master.ConsoleMaster, cmdline.mitmproxy, args)
    return None


def mitmdump(*args):
    def extra(args):
        if args.filter_args:
            v = " ".join(args.filter_args)
            return dict(
                save_stream_filter=v,
                readfile_filter=v,
                dumper_filter=v,
            )
        return {}

    m = run(dump.DumpMaster, cmdline.mitmdump, args, extra)
    if m and m.errorcheck.has_errored:  # type: ignore
        return 1
    return None


if __name__ == '__main__':
    mitmproxy()
