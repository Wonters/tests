import asyncio
import uvicorn
from fastapi import FastAPI, WebSocket, BackgroundTasks, Request, Depends
from fastapi.websockets import WebSocketDisconnect

from fastapi.templating import Jinja2Templates
from fastapi.testclient import TestClient
from typing import List

app = FastAPI()
templates = Jinja2Templates('.')

back_ground_tasks = BackgroundTasks()


def get_background_tasks():
    try:
        yield back_ground_tasks
    finally:
        pass


class Service:

    def __init__(self, name: str, queue: asyncio.Queue):
        self.name = name
        self.queue = queue

    async def start(self):
        print('start service')
        await self.queue.put(f'service {self.name} start')
        for m in range(20):
            await self.queue.put(m)
            await asyncio.sleep(1)
        print("service finish")

    async def get_from_queue(self):
        while not self.queue.empty():
            m = await self.queue.get()
            yield m


class ConnectionManager:

    def __init__(self):
        self.actives_websockets: List[WebSocket] = []

    async def connexion(self, ws: WebSocket):
        # if ws in self.actives_websockets:
        #     print(f"socket {ws} already presente --> disconnect first")
        #     self.deconnexion(ws)
        # else:
        await ws.accept()
        self.actives_websockets.append(ws)

    def deconnexion(self, ws):
        self.actives_websockets.remove(ws)

    async def send_private_message(self, message: str, ws: WebSocket):
        await ws.send_text(message)

    async def broadcast(self, message: str):
        for ws in self.actives_websockets:
            await ws.send_text(message)


connexion_manager = ConnectionManager()

def launch(service: Service):
    BackgroundTasks().add_task(service.start)
    back_ground_tasks.add_task(service.get_from_queue)

@app.get('/service')
async def launch_service(name: str, bg_t: BackgroundTasks, bg_q=Depends(get_background_tasks)):
    q = asyncio.Queue(maxsize=10)
    service = Service(name, queue=q)
    bg_t.add_task(service.start)
    bg_q.add_task(service.get_from_queue)
    #launch(service)


@app.websocket('/ws')
async def send_stdout(ws: WebSocket, bg_tasks=Depends(get_background_tasks)):
    await connexion_manager.connexion(ws)
    try:
        while True:
            for task in bg_tasks.tasks:
                async for message in task.func():
                    await connexion_manager.broadcast(str(message))
            await asyncio.sleep(0.1)
    except Exception as e:
        print(f"Deconnexion for websocket {ws}")
        connexion_manager.deconnexion(ws)


@app.get('/view1')
def view_1(request: Request):
    return templates.TemplateResponse('view1.html', context={'request': request})


@app.get("/view2")
def view_2():
    return "welcome to another view"


if __name__ == "__main__":
    uvicorn.run(app="exercice:app", host="127.0.0.1", port=8000, reload=True)
