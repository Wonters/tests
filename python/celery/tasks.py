import urllib3
import smtplib

from collections import Counter

from celery.schedules import crontab
from celery.decorators import periodic_task
from celery import Celery

celery = Celery('tasks', broker='redis://localhost', backend='redis://localhost')


@celery.task
def ecount(url):
    http = urllib3.PoolManager()
    return Counter(http.request('GET', url))['e']


@periodic_task(run_every=crontab(hour='5,13,23', minute=30, day_of_week='monday'))
def is_alive():
    """
    Vérifie que le blog est toujours en ligne, et si ce n'est pas le cas, envoie un mail en panique
    """
    http = urllib3.PoolManager()
    if http.request('GET', 'http://sametmax.com').code != 200:
        mail = 'tristan.herou__AT__gmail.com'.replace('__AT__', '@')
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login('root', 'admin123')
        server.sendmail(mail, mail, msg)
        server.quit()
