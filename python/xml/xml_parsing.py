from lxml import etree
import xmltodict
import json
class parsingXml:

    def __init__(self, xml:etree.fromstring):
            self.xml = xml
            self.params = None 

    def get_kvp(self):
        for node in self.xml.iter():
            self.params[node.xpath("local-name(.)")] = node.text 

    def get_keys(self):
        return self.params.keys()

    def find_node(self, node):
        if node in self.get_keys():
            return self.params[node]
        else:
            return None

    def get_local_name(self, node):
        return node.xpath("local-name(.)")

    def convert_xml_json(self, xml_string):
        json_ = json.dumps(xmltodict.parse(xml_string))
        return json_


if __name__ == "__main__":
    file = open("wcs.xml", "r", encoding='UTF-8')
    xml_string = file.read()
    xml = etree.fromstring(xml_string.encode(encoding='UTF-8'))
    parsxml = parsingXml(xml)
    result = parsxml.convert_xml_json(xml_string)
    print(result)