#!/bin/bash

iteration=0


while test $iteration -lt 100
do
	echo "ligne $iteration" >> test_
	git add -u
	git commit -m "commit $iteration"
	iteration=$(($iteration+1))
	sleep 0.2
done
exit 0


